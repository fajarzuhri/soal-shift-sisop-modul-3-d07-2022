#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

int main(){	

	int socket_desc , client_sock , c , read_size;
	struct sockaddr_in server , client;
	char username[1000] , server_reply[2000], password[1000];
	char clientLogin[1024], user[1024];
	char checkUsername[1024], checkPassword[1024];
    // int choice;
	uint32_t tmp,choice;
	FILE *file;

	//Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( 8888 );
	
	//Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		//print the error message
		perror("bind failed. Error");
		return 1;
	}
	puts("bind done");
	
	//Listen
	listen(socket_desc , 3);
	
	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
	
	//accept connection from an incoming client
	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
	if (client_sock < 0)
	{
		perror("accept failed");
		return 1;
	}
	puts("Connection accepted");
	
	//Receive a message from client
	while(1){

		read(client_sock, &tmp, sizeof(tmp));
		choice = ntohl(tmp);

		// read_size = read(client_sock, choice, 1024);
   		read_size = read(client_sock, username, 1024);
    	read_size = read(client_sock, password, 1024);

		if(choice == 1){

			file = fopen("user.txt", "a+");

			if (file == NULL)
				exit(1);

			strcpy(user, username);
			strcat(user,":");
			strcat(user, password);
			fputs(user, file);
			fprintf(file, "\n");

			send (client_sock, "Register Berhasil!!!\n\n", 100, 0);
			fclose(file);
		}

		else if( choice == 2){
			
			char check[1024];
			bool signal = false;
			file = fopen("user.txt", "r");

			if (file == NULL) 
				exit(1);

			while(file != NULL && fgets(check, sizeof(check), file) != NULL){

				int i = 0;
				int j = 0;

				for (int i = 0; check[i] != ':' && check[i] != '\n'; i++){

					checkUsername[i] = check[i];
					checkPassword[j] = check[i];
				j++;
				}

				checkUsername[i]; 
				i++;

				checkPassword[j]; 
				j++;
			
				if (strcmp(username, checkUsername) == 0 && strcmp(password, checkPassword) == 0) 
					signal = true;
				
			}

			fclose(file);

			if (signal = true){
				send (client_sock, "1", 20, 0);
			}else {
				send(client_sock, "0", 20,0);
			}
			
		}

		file = fopen("problem.tsv", "a+");
		fclose(file);
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
	}
	
	return 0;
}
