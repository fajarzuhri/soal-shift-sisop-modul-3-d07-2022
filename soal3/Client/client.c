#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(){
  pid_t cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8, cid9, cid10, cid11, cid12;
  int status;
    
///////////////////////////////////////////////////////////////////////////////////

	cid1 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid1 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid1 == 0){
	char *arg[]={"find", "/home/hadi/modul3", "-type", "f", "-name", "hartakarun.zip", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk menghapus file hartakarun.zip
	} else{
	while((wait(&status)) > 0);
	}
    
///////////////////////////////////////////////////////////////////////////////////	

	cid2 = fork(); // digunakan untuk membuat child proses ketiga
	if(cid2 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}
	if(cid2 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-exec", "zip", "-r", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk mengzip folder hartakarun
	} else{
	while((wait(&status)) > 0);
	}
    
///////////////////////////////////////////////////////////////////////////////////	

	cid3 = fork(); // digunakan untuk membuat child proses ketiga
	if(cid3 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid3 == 0){
	char *argv[] = {"scp", "-r", "hartakarun.zip", "server@192.168.100.139: /home/server/", NULL};
	execv("/usr/bin/scp", argv); // digunakan untuk mengirim file hartakarun.zip ke server
	} else{
	while((wait(&status)) > 0);
	}
}
