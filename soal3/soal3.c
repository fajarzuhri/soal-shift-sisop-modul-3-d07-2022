#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(){
  pid_t cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8, cid9, cid10, cid11, cid12, cid13, cid14, cid15, cid16, cid17, cid18, cid19, cid20, cid21, cid22, cid23, cid24, cid25;
  int status;
    
///////////////////////////////////////////////////////////////////////////////////

    
	cid1 = fork(); // digunakan untuk membuat child proses ketiga
	if(cid1 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid1 == 0){
	char *argv[] = {"unzip", "-q", "hartakarun.zip", NULL};
	execv("/usr/bin/unzip", argv); // digunakan untuk mengunzip file hartakarun.zip
	} else{
	while((wait(&status)) > 0);
	}
    
///////////////////////////////////////////////////////////////////////////////////

	cid2 = fork(); // digunakan untuk membuat child proses pertama
	if(cid2 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid2 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/Unknow", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
   
///////////////////////////////////////////////////////////////////////////////////

	cid3 = fork(); // digunakan untuk membuat child proses pertama
	if(cid3 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid3 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/Hidden", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Hidden
	} else{
	while((wait(&status)) > 0);
	}
   
///////////////////////////////////////////////////////////////////////////////////

	cid4 = fork(); // digunakan untuk membuat child proses keempat
	if(cid4 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}        
	if(cid4 == 0){ 
	char *argv[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-exec", "mv", "-t", "/home/hadi/modul3/Unknow", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",argv); // digunakan untuk memindahkan seluruh isi file hartakarun ke folder unknow
	} else{
	while((wait(&status)) > 0);
	}
   
///////////////////////////////////////////////////////////////////////////////////

	cid5 = fork(); // digunakan untuk membuat child proses keempat
	if(cid5 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}        
	if(cid5 == 0){ 
	char *argv[]={"find", "/home/hadi/modul3/Unknow", "-type", "f", "-name", ".DS_Store", "-exec", "mv", "-t", "/home/hadi/modul3/Hidden", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",argv); // digunakan untuk memindahkan file hidden ke folder hidden
	} else{
	while((wait(&status)) > 0);
	}
   
///////////////////////////////////////////////////////////////////////////////////

	cid6 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid6 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid6 == 0){
	char *arg[]={"find", "/home/hadi/modul3/Unknow", "-type", "f", "-name", "*.*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid7 = fork(); // digunakan untuk membuat child proses pertama
	if(cid7 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid7 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/jpg", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid8 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid8 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid8 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*jpg", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/jpg", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid9 = fork(); // digunakan untuk membuat child proses pertama
	if(cid9 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid9 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/png", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid10 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid10 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid10 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*png*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/png", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid11 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid11 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid11 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*PNG*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/png", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid12 = fork(); // digunakan untuk membuat child proses pertama
	if(cid12 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid12 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/txt", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid13 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid13 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid13 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*txt*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/txt", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid14 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid14 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid14 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*TXT*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/txt", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid15 = fork(); // digunakan untuk membuat child proses pertama
	if(cid15 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid15 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/zip", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid16 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid16 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid16 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*zip*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/zip", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid16 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid16 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid16 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*ZIP*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/zip", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid17 = fork(); // digunakan untuk membuat child proses pertama
	if(cid17 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid17 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/gif", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid16 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid16 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid16 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*gif*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/gif", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid17 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid17 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid17 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*missing*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/gif", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid18 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid18 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid18 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*JPG*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/jpg", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid19 = fork(); // digunakan untuk membuat child proses pertama
	if(cid17 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid19 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/wiz", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid20 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid20 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid20 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*wiz*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/wiz", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid21 = fork(); // digunakan untuk membuat child proses pertama
	if(cid21 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid21 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/jpeg", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid22 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid22 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid22 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*jpeg*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/jpeg", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid23 = fork(); // digunakan untuk membuat child proses pertama
	if(cid23 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid23 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/pdf", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*pdf*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/pdf", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/tar.gz", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*tar.gz*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/tar.gz", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/js", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.js*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/js", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.JS*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/js", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/sh", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.sh*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/sh", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}	
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/c", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.c*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/c", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/hex", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.hex*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/hex", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/bin", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.bin*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/bin", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
	
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/fbx", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
		
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*.fbx*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/fbx", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
		
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/_", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
			
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*._*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/_", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
			
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/rmmzsave", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
				
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*rmmzsave*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/rmmzsave", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}
				
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses pertama
	if(cid24 < 0){
	exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
	}

	if(cid24 == 0){
	char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/hartakarun/gns3project", NULL};
	execv("/bin/mkdir", argv); // digunakan untuk membuat folder Unknow
	} else{
	while((wait(&status)) > 0);
	}
					
///////////////////////////////////////////////////////////////////////////////////

	cid24 = fork(); // digunakan untuk membuat child proses kedelapan
	if(cid24 < 0){
	exit(EXIT_FAILURE); // jika gagal membuat proses baru, maka program akan berhenti
	}
	if(cid24 == 0){
	char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-name", "*gns3project*", "-exec", "mv", "-t", "/home/hadi/modul3/hartakarun/gns3project", "{}", "+", (char *) NULL};
	execv("/usr/bin/find",arg); // digunakan untuk memindahkan file yang berekstensi
	} else{
	while((wait(&status)) > 0);
	}








	
	
}







