# soal-shift-sisop-modul-3-D07-2022

Praktikum 3 Sistem Operasi 2022

## Anggota Kelompok
- Fitra Agung Diassyah Putra - 5025201072
- Ananda Hadi Saputra - 5025201148
- Fajar Zuhri Hadiyanto - 5025201248

## Soal 1

Pertama-tama, import dulu library yang diperlukan
```CPP
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<dirent.h>

// USING EXTERNAL LIBRARY
#include "deps/b64/b64.h"
```

Setelah itu, siapkan fungsi dan id untuk masing-masing thread yang akan digunakan
```CPP
pthread_t dtid[2];
pthread_t etid[2];
pthread_t dectid[2];
pthread_t mvtid[2];

void* download_file(void *arg);
void* extract_zip(void* arg);
void* decode_txt(void *arg);
void* move_output_file(void * arg);
void* unzip (void * arg);
void* create_file_no (void * arg);
```

Pada main, kita inisialisasikan dulu data-data yang diperlukan, seperti nama file, folder, link untuk download dan juga panjang datanya
```CPP
// DATA FOR DOWNLOADING ZIP FILE
char data[2][100] = {
        "music.zip|https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1",
        "quote.zip|https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt"
};

// DATA FOR FOLDER AND FILE NAME
char name[2][20] = {"music", "quote"};

int len = sizeof(dtid) / sizeof(dtid[0]);
int err;
```

Buat Thread untuk Download 2 file secara bersamaan, lalu tunggu hingga keduanya selesai
```CPP
// CREATE PARALLEL THREAD FOR DOWNLOAD 2 ZIP FILES
for (int i = 0; i < len; ++i) {
    err = pthread_create(&(dtid[i]), NULL, &download_file, (void *) data[i]);

    if (err != 0) {
        printf("%d - ERROR DOWNLOAD ZIP\n", err);
    }
}

// WAIT FOR BOTH FILE TO BE DOWNLOADED
pthread_join(dtid[0], NULL);
pthread_join(dtid[1], NULL);
```

Berikut ini merupakan thread function untuk download
```CPP
void* download_file(void *arg) {
    char *data = (char *) arg;
    char *filename, *link;

    filename = strtok(data, "|");
    link = strtok(NULL, "|");

    char *argv[] = {"wget", "-O", filename, link, "-q", NULL};

    pid_t child = fork();
    int status;

    if (child < 0) {
        puts("ERROR FORKING THREAD DOWNLOAD");
    } else if (child == 0) {
        execv("/usr/bin/wget", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}
```

Setelah kedua proses download selesai, buat thread untuk unzip kedua file tersebut secara bersamaan
```CPP
// CREATE PARALLEL THREAD FOR UNZIP 2 DOWNLOADED FILES
for (int i = 0; i < len; ++i) {
    err = pthread_create(&(etid[i]), NULL, &extract_zip, (void *) name[i]);

    if (err != 0) {
        printf("%d - ERROR EXTRACT ZIP\n", err);
    }
}

// WAIT FOR BOTH FILE TO BE UNZIPPED
pthread_join(etid[0], NULL);
pthread_join(etid[1], NULL);
```

Berikut ini merupakan thread function untuk unzip
```CPP
void* extract_zip(void* arg) {
    char *foldername = (char*) arg;

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR FORKING THREAD EXTRACT");
    } else if (child == 0) {
        char *argv[] = {"unzip", "-q", foldername, "-d", foldername, NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}
```

Setelah kedua file diekstrak, maka buat thread untuk decode semua file yang ada di dalam kedua folder tersebut
```CPP
// CREATE PARALLEL THREAD FOR DECODED EACH FOLDER CONTENTS
for (int i = 0; i < len; ++i) {
    err = pthread_create(&(dectid[i]), NULL, &decode_txt, (void *) name[i]);

    if (err != 0) {
        printf("%d - ERROR DECODE FILE\n", err);
    }
}

// WAIT FOR ALL FILES IN BOTH FOLDER TO BE DECODED
pthread_join(dectid[0], NULL);
pthread_join(dectid[1], NULL);
```

Berikut ini merupakan thread function untuk decode file txt
```CPP
void* decode_txt(void *arg) {
    char foldername[100], output_filename[100], cwd[256];
    getcwd(cwd, sizeof(cwd));
    strcpy(foldername, cwd);
    strcat(foldername, "/");
    strcat(foldername, (char *) arg);

    strcpy(output_filename, foldername);
    strcat(output_filename, "/");
    strcat(output_filename, (char *) arg);
    strcat(output_filename, ".txt");

    int status;
    DIR *dp;
    struct dirent *ep;
    FILE *fp_input, *fp_output;
    char line[128], input_filename[100];
    unsigned char *output_line;

    fp_output = fopen(output_filename, "w");

    dp = opendir(foldername);
    if (dp != NULL) {
        while((ep = readdir(dp))) {
            if (strcmp(ep->d_name, ".") && strcmp(ep->d_name, "..")) {
                strcpy(input_filename, foldername);
                strcat(input_filename, "/");
                strcat(input_filename, ep->d_name);

                if (!strcmp(input_filename, output_filename)) continue;

                fp_input = fopen(input_filename, "r");
                if (fp_input != NULL) {
                    while (fgets(line, sizeof(line), fp_input) != NULL) {
                        output_line = b64_decode(line, strlen(line));
                        fprintf(fp_output, "%s\n", output_line);
                    }
                }
                fclose(fp_input);
            }
        }
    }
    fclose(fp_output);
    pthread_exit(NULL);
}
```

Setelah semua file didecode, buat proses baru untuk membuat folder hasil
```CPP
// CREATE NEW PROCESS TO CREATE NEW FOLDER AND WAIT UNTIL FINISHED
pid_t child = fork();
int status;
if (child < 0) {
    puts("ERROR FORKING CREATE FOLDER HASIL");
} else if (child == 0) {
    char *argv[] = {"mkdir", "-p", "hasil", NULL};
    execv("/usr/bin/mkdir", argv);
} else {
    waitpid(child, &status, 0);
}
```

Setelah folder berhasil dibuat, buat thread baru untuk memindahkan file hasil dari kedua folder ke dalam folder hasil
```CPP
// CREATE PARALLEL THREAD TO MOVE EACH RESULT FILE TO NEW FOLDER
for (int i = 0; i < len; ++i) {
    err = pthread_create(&(mvtid[i]), NULL, &move_output_file, (void *) name[i]);

    if (err != 0) {
        printf("%d - ERROR MOVE OUTPUT FILE\n", err);
    }
}

// WAIT FOR BOTH FILES TO BE MOVED
pthread_join(mvtid[0], NULL);
pthread_join(mvtid[1], NULL);
```

Berikut ini merupakan thread untuk memindahkan file
```CPP
void* move_output_file(void * arg) {
    char foldername[100], cwd[256], input_file[100], output_file[100];
    getcwd(cwd, sizeof(cwd));
    strcpy(foldername, cwd);
    strcat(foldername, "/");
    strcat(foldername, (char *) arg);

    strcpy(input_file, foldername);
    strcat(input_file, "/");
    strcat(input_file, (char *) arg);
    strcat(input_file, ".txt");

    strcpy(output_file, cwd);
    strcat(output_file, "/hasil/");
    strcat(output_file, (char *) arg);
    strcat(output_file, ".txt");

    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR FORKING CREATE FOLDER HASIL");
    } else if (child == 0) {
        char *argv[] = {"mv", input_file, output_file, NULL};
        execv("/usr/bin/mv", argv);
    } else {
        waitpid(child, &status, 0);
    }

    pthread_exit(NULL);
}
```

Setelah kedua file dipindahkan, zip folder hasil dan hapus folder tersebut
```CPP
// CREATE NEW PROCESS TO ZIP NEW FOLDER AND WAIT UNTIL FINISHED
child = fork();
if (child < 0) {
    puts("ERROR FORKING ZIP FOLDER HASIL");
} else if (child == 0) {
    char *argv[] = {"zip", "--password", "mihinomenestfajar", "-r", "hasil.zip", "hasil", NULL};
    execv("/usr/bin/zip", argv);
} else {
    waitpid(child, &status, 0);
}

// CREATE NEW PROCESS TO REMOVE NEW FOLDER AS IT IS ZIPPED
child = fork();
if (child < 0) {
    puts("ERROR REMOVE FOLDER HASIL");
} else if (child == 0) {
    char *argv[] = {"rm", "-r", "hasil", NULL};
    execv("/usr/bin/rm", argv);
} else {
    waitpid(child, &status, 0);
}
```

Setelah itu, buat dua thread untuk unzip folder dan create file baru secara bersamaan
```CPP
// CREATE NEW PARALLEL THREAD TO UNZIP THAT FILE AND CREATE NEW FILE
pthread_t unzip_tid;
pthread_t makefile_tid;

err = pthread_create(&unzip_tid, NULL, &unzip, NULL);
if (err != 0) {
    printf("%d - ERROR UNZIP\n", err);
}

err = pthread_create(&makefile_tid, NULL, &create_file_no, NULL);
if (err != 0) {
    printf("%d - ERROR CREATE FILE NO\n", err);
}

// WAIT UNTIL BOTH PROCESS FINISHED
pthread_join(unzip_tid, NULL);
pthread_join(makefile_tid, NULL);
```

Berikut ini merupakan thread untuk unzip
```CPP
void* unzip (void * arg) {
    pid_t child = fork();
    int status;
    if (child < 0) {
        puts("ERROR FORKING CREATE FOLDER HASIL");
    } else if (child == 0) {
        char *argv[] = {"unzip", "-P", "mihinomenestfajar", "hasil.zip", NULL};
        execv("/usr/bin/unzip", argv);
    } else {
        waitpid(child, &status, 0);
    }
    pthread_exit(NULL);
}
```

dan berikut ini merupakan thread untuk create file baru
```CPP
void* create_file_no (void * arg) {
    FILE *fp;
    fp = fopen("no.txt", "w");
    fputs("No", fp);
    fclose(fp);
    pthread_exit(NULL);
}
```

Setelah Dua proses tersebut berhasil, zip kembali folder tersebut dan hapus folder dan filenya
```CPP
// CREATE NEW PROCESS TO REZIP THE FOLDER AND NEW FILE
child = fork();
if (child < 0) {
    puts("ERROR FORKING ZIP FOLDER HASIL");
} else if (child == 0) {
    char *argv[] = {"zip", "--password", "mihinomenestfajar", "-r", "hasil.zip", "hasil", "no.txt", NULL};
    execv("/usr/bin/zip", argv);
} else {
    waitpid(child, &status, 0);
}

// REMOVE THE FOLDER AGAIN AS IT IS REZIPPED
child = fork();
if (child < 0) {
    puts("ERROR REMOVE FOLDER HASIL");
} else if (child == 0) {
    char *argv[] = {"rm", "-r", "hasil", NULL};
    execv("/usr/bin/rm", argv);
} else {
    waitpid(child, &status, 0);
}

// REMOVE THE NEW FILE AS IT IS ZIPPED
child = fork();
if (child < 0) {
    puts("ERROR REMOVE FOLDER HASIL");
} else if (child == 0) {
    char *argv[] = {"rm", "no.txt", NULL};
    execv("/usr/bin/rm", argv);
} else {
    waitpid(child, &status, 0);
}
```

Berikut ini merupakan struktur folder sebelum eksekusi

![Gambar 1](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/1.png)

Berikut ini struktur folder setelah script soal1 dieksekusi

![Gambar 2](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/2.png)

Berikut ini merupakan isi dari file hasil.zip yang telah diekstrak ke dalam folder hasil

![Gambar 3](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/3.png)
![Gambar 4](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/4.png)

Berikut ini merupakan isi dari masing-masing file

![Gambar 5](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/5.png)
![Gambar 6](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/6.png)
![Gambar 7](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-1/7.png)

## Soal 2

### Client

Import terlebih dahulu library yang dibutuhkan 
```CPP
#include <stdio.h>	
#include <string.h>	
#include <sys/socket.h>	
#include <arpa/inet.h>	
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
```

pada awal main, inisialisasikan dulu data-data yang diperlukan
```CPP
	int sock, n, read_size;
	struct sockaddr_in server;
	char username[1000] , server_reply[2000], password[1000];
	char clientLogin[1024],user[1024] = {0}, buffer[1024];
	uint32_t tmp,choice;
```

Buat Socket
```CPP
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");
```

Siapkan untuk struktur sockaddr_in
```CPP
	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons( 8888 );
```

Connect ke Server
```CPP
	if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("connect failed. Error");
		return 1;
	}
	
	puts("Connected\n");
```

akan terus berkomunikasi dengans server saat masih didalam while
```CPP
    while(1){
        //your code here
    }
```

diawal kita akan disajikan menu, dan diminta untuk memilih dan pilihan tersebut akan dikirimkan ke server
```CPP
printf("Main Menu");
printf("\n 1. Register");
printf("\n 2. Login");
printf("\n Enter your choice\n");
scanf("%d",&choice);

tmp = htonl(choice);
write(sock, &tmp, sizeof(tmp));
```

kemudian dari pilhan tadi akan massuk ke switch(choice)
```CPP
switch(choice){
    // your case i here
}
```

pada case 1 kita akan meregistrasikan akun dan mengirim username dan password nya ke server
```CPP
case 1:
    printf("Welcome to Register page\n");
    printf("Enter Your Username : ");
    scanf("%s" , username);
    send(sock , username , strlen(username) , 0);
        
    printf("Enter your Password : ");
    scanf("%s", password);
    send(sock , password , strlen(password) , 0);

    printf("Register Berhasil!!!\n\n");

    break;
```

pada case 2 kita akan memasukkan username dan password yang tersedia untuk login,
kemudian kita akan menerima signal dari server dan kemudian membaca signal tersebut apakah username dan password tersebut sama
```CPP
case 2: 
    printf("Welcome to Login page\n");
    printf("Enter Your Username : ");
    scanf("%s" , username);
    send(sock , username , strlen(username) , 0);

    printf("Enter your Password : ");
    scanf("%s", password);
    send(sock , password , strlen(password) , 0);

    read_size = read(sock, user, 1024);

    if (strcmp(user, "0") == 0) 
        printf("ID/Password Salah");

    if (strcmp(user, "1") == 0){
        printf("Login Berhasil\n");
        break;
    }
    memset(buffer, 0, 1024); 
    break;

```

### Server

Pertama import dulu library yang dibutuhkan 
```CPP
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
```

pada awal main, inisialisasikan dulu data-data yang diperlukan
```CPP
    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;
    char username[1000] , server_reply[2000], password[1000];
    char clientLogin[1024], user[1024];
    char checkUsername[1024], checkPassword[1024];
```

Buat Socket
```CPP
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");
```

Siapkan untuk struktur sockaddr_in
```CPP
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( 8888 );
```

Setelah itu di bind
```CPP
if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		//print the error message
		perror("bind failed. Error");
		return 1;
	}
	puts("bind done");
```

jika sudah terbind maka server akan melisten dan mengaccept koneksi dari client
```CPP
listen(socket_desc , 3);


puts("Waiting for incoming connections...");
c = sizeof(struct sockaddr_in);


client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
if (client_sock < 0)
{
    perror("accept failed");
    return 1;
}
puts("Connection accepted");
```

akan terus berkomunikasi dengans server saat masih didalam while
```CPP
    while(1){
        //your code here
    }
```

pada awal while server akan selalu membaca choice, username, dan passsword yang dikirimkan client
```CPP
read(client_sock, &tmp, sizeof(tmp));
choice = ntohl(tmp);

read_size = read(client_sock, username, 1024);
read_size = read(client_sock, password, 1024);
```

jika choice = 1, maka akan membuat file bernama user.txt dan menginputkan username dan password
dengan format username:password. setelah itu akan close file
```CPP
file = fopen("user.txt", "a+");

if (file == NULL)
    exit(1);

strcpy(user, username);
strcat(user,":");
strcat(user, password);
fputs(user, file);
fprintf(file, "\n");

send (client_sock, "Register Berhasil!!!\n\n", 100, 0);
fclose(file);
```

jika choice = 2, diawal kita akan inisialisasikan lagi data-data yang diperlukan dan membuka file user.txt
```CPP
char check[1024];
bool signal = false;
file = fopen("user.txt", "r");

if (file == NULL) 
    exit(1);
```

setelah itu akan mengecek setiap line nya apakah username dan password yang diinput itu sama seperti yang ada di user.txt
jika sama maka signal akan di set menjadi true dan setelah itu menclose file user.txt
```CPP
while(file != NULL && fgets(check, sizeof(check), file) != NULL){

    int i = 0;
    int j = 0;

    for (int i = 0; check[i] != ':' && check[i] != '\n'; i++){

        checkUsername[i] = check[i];
        checkPassword[j] = check[i];
    j++;
    }

    checkUsername[i]; 
    i++;

    checkPassword[j]; 
    j++;

    if (strcmp(username, checkUsername) == 0 && strcmp(password, checkPassword) == 0) 
        signal = true;
    
}
fclose(file);
```

kemudian, jika signal true server akan mengirimkan 1 dan jika signal = false maka server akan mengirimkan 
```CPP
if (signal = true){
    send (client_sock, "1", 20, 0);
}else {
    send(client_sock, "0", 20,0);
}
```

server juga akan langsung membuat file bernama problem.tsv saat program dijalankan 
```CPP
file = fopen("problem.tsv", "a+");
	fclose(file);
```

berikut ini saat menginputkan username dan password untuk register

![soal2-3](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-2/3.jpg)

berikut ini list dari file saat program dijalankan dan telah menginput register

![soal2-2](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-2/2.jpg)

berikut ini adalah isi dari file user.txt

![soal2-4](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-2/4.jpg)

berikut ini saat menginputkan username dan password untuk login

![soal2-1](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-2/1.jpg)


## Soal 3

### Soal 3

Import terlebih dahulu library yang dibutuhkan

```cpp
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>
```

Untuk mengunzip file digunakan command ("unzip")
```cpp
char *argv[] = {"unzip", "-q", "hartakarun.zip", NULL};
execv("/usr/bin/unzip", argv);
```

Untuk membuat folder digunakan ("mkdir")
```cpp
char *argv[] = {"mkdir", "-p", "/home/hadi/modul3/folder", NULL};
execv("/bin/mkdir", argv);
```

Untuk memindahkan isi folder digunakan ("mv"), sebelum itu kita harus mengetahui lokasi file tersebut menggunakan ("find")
```cpp
char *argv[]={"find", "/home/hadi/modul3/hartakarun", "-type", "f", "-exec", "mv", "-t", "/home/hadi/modul3/folder tujuan", "{}", "+", (char *) NULL};
execv("/usr/bin/find",argv);
```

Untuk menghapus file / folder digunakan ("rm"), sebelum itu kita harus mengetahui lokasi file tersebut menggunakan ("find")
```cpp
char *arg[]={"find", "/home/hadi/modul3", "-type", "f", "-name", "hartakarun.zip", "-exec", "rm", "-r", "{}", "+", (char *) NULL};
execv("/usr/bin/find",arg);
```

Untuk mengzip folder digunakan ("zip"), sebelum itu kita harus mengetahui lokasi file tersebut menggunakan ("find")
```cpp
char *arg[]={"find", "/home/hadi/modul3/hartakarun", "-exec", "zip", "-r", "{}", "+", (char *) NULL};
execv("/usr/bin/find",arg);
```

Untuk mengirim hasil file ke server menggunakan ("scp")
```cpp
char *argv[] = {"scp", "-r", "hartakarun.zip", "server@192.168.100.139: /home/server/", NULL};
execv("/usr/bin/scp", argv);
```

Urutan dalam mengoprasikan file
1. cd home/hadi/modul3
2. gcc soal3.c -o soal3
3. ./soal3
4. gcc client.c -o client
5. ./client 
6. masukan pasword untk mengirim file ke server]

Berikut struktur pada saat setelah eksekusi

![soal3-1](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-3/1.png)

Berikut ini merupakan hasil eksekusi client.c , wajib memasukan pasword untuk mengirim file ke server

![soal3-2](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-3/2.png)

Berikut ini merupakan hasil file yang telah dikirim client ke server

![soal3-3](https://sisop.s3.ap-southeast-1.amazonaws.com/praktikum-3/soal-3/3.png)
